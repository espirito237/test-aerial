$(document).ready(function () {
    // save comment to database
    $("form").submit(function (e) {
        e.preventDefault();
        var form_url = $(this).attr("action"); //récupérer l'URL du formulaire
        var form_method = $(this).attr("method"); //récupérer la méthode
        var form_data = new FormData(this);
        var errorMessage = "Une erreur est survenue, veuillez recommencer"

        $.ajax({
            url: form_url,
            type: form_method,
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                
                //If added success show data
                if (response.message === "ok") {
                    $("#error").empty();
                    var table = $('#datatable').DataTable();
                    table.row.add([response.name, response.price]).draw(false);
                    $("#modal").modal('hide');

                } else {
                    
                    $("#error").append(errorMessage);
                }
            },

            error: function (jqXHR, textStatus, errorThrown) {
                $("#error").append(errorMessage);
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            },
        });
    });
})