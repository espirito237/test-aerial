Installation du projet

1) Git clone du projet.
2) Entrez dans le répertoire du projet et exécutez la commande : composer install.
3) Mettre à jour la variable environnement DATABASE_URL dans le fichier.env avec identifiants de connexion 
à votre base de données.
4) Créez la base de données du projet via la commande : php bin/console doctrine:database:create
5) Chargez les tables du projet via la commande php bin/console doctrine:schema:update --force
6) Démarrez le serveur : symfony server:start
7) Accédez au projet
