<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Service\FileUploader;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    /**
     * @Route("/", name="app_product")
     */
    public function index(ProductRepository $productRepository): Response
    {
        $products = $productRepository->findAll();
        $form = $this->createForm(ProductType::class);

        return $this->render('product/index.html.twig', [
            "products" => $products,
            "form" => $form->createView()
        ]);
    }

     /**
     * @Route("/ajout-produit", name="add_product", condition="request.isXmlHttpRequest()")
     */
    public function addProduct(
        Request $request, 
        EntityManagerInterface $entityManager, 
        FileUploader $fileUploader): Response
    {   
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            
            $image = $form->get('image')->getData();
            $price  = $form->get('price')->getData();
            $imageName = $fileUploader->upload($image);
            $product->setImage($imageName);
            $product->setTva(20);
            $product->setTotalPrice($price + ($price * (20/100)));
            $entityManager->persist($product);
            $entityManager->flush();
            
            return new JsonResponse([
                "message" => "ok",
                "name" => $product->getName(),
                "price" => $product->getTotalPrice(),
            ]);
        }

        return new JsonResponse(["message" => "ko"]);
    }
}
