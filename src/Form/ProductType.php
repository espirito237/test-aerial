<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                "label" => false,
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "nom"
                ]
            ])
            ->add('description', TextareaType::class, [

                "label" => false,
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "description"
                ]
            ])

            ->add('price', IntegerType::class, [
                "label" => false,
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "Prix Ht"
                ]
            ])
            ->add('image', FileType::class, [
                "label" => "Image du produit",
                "attr" => [
                    "class" => "form-control",
                    "accept" => "image/png, jpg, jpeg",
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                    ])
                ],
                "mapped" => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'id' => 'form'
        ]);
    }
}
